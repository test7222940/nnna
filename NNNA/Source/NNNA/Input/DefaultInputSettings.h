// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InputAction.h"
#include "DefaultInputSettings.generated.h"

/**
 * 
 */
UCLASS()
class NNNA_API UDefaultInputSettings : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:

	/** Camera move Input Action */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UInputAction* CameraAction;

	/** Zoom Input Action */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UInputAction* ZoomAction;

	/** Jump Input Action */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UInputAction* MoveAction;

	/** Look Input Action */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UInputAction* LookAction;



	UInputAction* GetCameraAction() const { return CameraAction; };
	UInputAction* GetZoomAction() const { return ZoomAction; };
	UInputAction* GetJumpAction() const { return JumpAction; };
	UInputAction* GetMoveAction() const { return MoveAction; };
	UInputAction* GetLookAction() const { return LookAction; };

};
